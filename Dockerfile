FROM python:3.9-alpine

ENV PYTHONUNBUFFERED=1
WORKDIR /code
#COPY requirements.txt /code/
#RUN pip install -r requirements.txt
COPY main.py /code/
ENTRYPOINT ["python", "main.py"]
